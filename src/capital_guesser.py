import sys
import random

def get_states_and_capitals(file_name):
    states_and_capitals = []
    with open(file_name, mode = 'rt') as states_and_capitals_file:  # opening file
        for data in states_and_capitals_file:
            state, capital = data.strip().split(",")
            states_and_capitals.append([state, capital])
    states_and_capitals_file.close();                               # closing file
    return states_and_capitals

file_name = sys.argv[1]
states_and_capitals = get_states_and_capitals(file_name)

# randomly select a state from the above list
state_capital = random.choice(states_and_capitals)
# get the user input
user_input_capital = input("what is the capital of %s? " %(state_capital[0]))

# compare the user input
if user_input_capital in state_capital:
    print("Yay! Great job!")
else:
    print("Sorry, that's incorrect.")
