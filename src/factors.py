import sys

# number = int(input("Enter number? "))
number = int(sys.argv[1])
factors = []

for i in range(1,number):
    if number % i == 0:
        factors.append(i)

if len(factors) > 1:
    for factor in factors:
        print(factor)
else:
    print("%d is prime!" %number)
