import sys

temprature = float(sys.argv[1])

if temprature in range(65,76):
    print("Just right")
elif( temprature > 75):
    print("Too hot. Gotta run for shelter")
else:
    print("Cold as ice")
