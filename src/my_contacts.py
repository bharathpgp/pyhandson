def store_contact(name,email,mobile,twitter):
    with open('contacts.csv', mode = 'a') as contact_file:
        contact_file.write("%s,%s,%s,%s\n" %(name, email, mobile, twitter))
    contact_file.close()

name = input("What is the contact name? ")
email = input("What is the %s email id? " % name )
mobile = input("What is the %s phone no? " % name )
twitter = input("What is the %s twitter handle? " % name)
store_contact(name, email, mobile, twitter)
