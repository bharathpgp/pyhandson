import random

proteins = ["ham", "turkey", "beef", "tofu"]
condiments = ["mustard", "mayo", "hummus"]
veggies = ["tomato", "lettuce", "onion", "sprouts"]
breads = ["white bread", "wheat bread", "pita bread", "sourdough"]
print(random.choice(proteins))
print(random.choice(condiments))
print(random.choice(veggies))
print(random.choice(breads))
