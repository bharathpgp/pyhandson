file_name = input('Enter file name? ')

def print_file_stats(file_name):
    with open(file_name, mode = 'rt') as file:
        data = file.read()
    file.close()
    wc = len(data.split())
    print("No of words in file: %d" %wc)

print_file_stats(file_name)
