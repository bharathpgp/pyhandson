import sys

num1, num2 = float(sys.argv[1]), float(sys.argv[2])
positive_difference = num1 - num2
if positive_difference < 0:
    positive_difference = positive_difference * -1
print(positive_difference)
